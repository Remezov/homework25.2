package ru.remezov.basket;

public class App {
    public static void main(String[] args) {
        ProductBasket basket = new ProductBasket();

        basket.addProduct("Cola", 10);
        basket.addProduct("Pepsi", 15);
        basket.addProduct("Mirinda", 20);
        basket.addProduct("Fanta", 10);

        //basket.clear();
        //basket.removeProduct("Cola");
        //basket.updateProductQuantity("Pepsi", 5);
        //System.out.println(basket.getProductQuantity("Pepsi"));

        System.out.println(basket.getProducts());
    }
}
