package ru.remezov.basket;

import java.util.*;

public class ProductBasket implements Basket{
    Map<String, Integer> products = new HashMap<>();

    @Override
    public void addProduct(String product, int quantity) {
        products.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        if (!products.containsKey(product)) {
            System.out.println("This product isn't found.");
            return;
        }
        products.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (!products.containsKey(product)) {
            System.out.println("This product isn't found.");
            return;
        }
        products.put(product, quantity);
    }

    @Override
    public void clear() {
        products.clear();
        System.out.println("The basket is empty");
    }

    @Override
    public List<String> getProducts() {
        if (products.isEmpty()) {
            System.out.println("The basket is empty.");
            return null;
        }
        List<String> productName = new ArrayList<>();
        productName.addAll(products.keySet());
        return productName;
    }

    @Override
    public int getProductQuantity(String product) {
        if (!products.containsKey(product)) {
            System.out.println("This product isn't found.");
            return 0;
        }
        return (int) products.get(product);
    }
}
